package com.ss.emome.entity.POJO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Myroslav on 23.01.2018.
 */
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
@AllArgsConstructor
public class Entertainment {

    String place_id;
    String name;
    String latitude;
    String longitude;

    //List<EntertainmentType> types = new ArrayList<>();

}
