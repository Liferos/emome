package com.ss.emome.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Myroslav on 28.12.2017.
 */
@FieldDefaults(level = AccessLevel.PRIVATE)
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "dish")
public class Dish {

    public Dish(String title, double price, int emoComp, int emoNum, Entertainment entertainment, String img) {
        this.title = title;
        this.price = price;
        this.emoComponent = emoComp;
        this.emoNum = emoNum;
        this.entertainment = entertainment;
        this.imageUrl = img;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    long id;

    @Column(name = "title")
    String title;

    @Column(name = "price")
    double price;

    @Column(name = "emotional_component")
    double emoComponent;

    @Column(name = "emo_num")
    int emoNum;

    @Column(name = "image_url")
    String imageUrl;

    @Column(name = "comment_num")
    int commentNum;

    @ManyToOne
    @JoinColumn(name = "entertainment_fk")
    Entertainment entertainment;

    @ManyToMany
    @JoinTable(name = "dish_type_relship",
               joinColumns = @JoinColumn(name = "dish_fk"),
               inverseJoinColumns = @JoinColumn(name = "type_fk"))
    List<DishType> dishTypes;

    @JsonIgnore
    @OneToMany(mappedBy = "dish")
    List<Comment> comments;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "image_fk")
    Image image;

}
