package com.ss.emome.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

/**
 * Created by Myroslav on 28.12.2017.
 */
@FieldDefaults(level = AccessLevel.PRIVATE)
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "comments")
public class Comment {

    public Comment(String text, String author, Dish dish) {
        this.text = text;
        this.author = author;
        this.dish = dish;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    long id;

    @Getter @Setter
    @Column(name = "text")
    String text;

    @Getter @Setter
    @Column(name = "author")
    String author;

    @JsonIgnore
    @Getter @Setter
    @ManyToOne
    @JoinColumn(name = "dish_fk")
    Dish dish;

}
