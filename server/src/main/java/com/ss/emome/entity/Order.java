package com.ss.emome.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * Created by Myroslav on 09.01.2018.
 */
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter
@Entity
@Table(name = "order_table")
public class Order {

    public Order(Timestamp dateTime, Entertainment entertainment) {
        this.dateTime = dateTime;
        this.entertainment = entertainment;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    long id;

    @Column(name = "date")
    Timestamp dateTime;

    @Column(name = "total")
    double total;

    @ManyToOne
    @JoinColumn(name = "entertainment_fk")
    Entertainment entertainment;

    @OneToMany(mappedBy = "order", fetch = FetchType.EAGER)
    List<OrderComponent> orderComponents;

}
