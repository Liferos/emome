package com.ss.emome.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

/**
 * Created by Myroslav on 25.01.2018.
 */
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "order_component")
public class OrderComponent {

    public OrderComponent(long amount, Dish dish, Order order) {
        this.amount = amount;
        this.dish = dish;
        this.order = order;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    long id;

    @Column(name = "amount")
    long amount;


    @OneToOne
    @JoinColumn(name = "dish_fk")
    Dish dish;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "order_fk")
    Order order;

}
