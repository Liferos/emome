package com.ss.emome.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Myroslav on 28.12.2017.
 */
@FieldDefaults(level = AccessLevel.PRIVATE)
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "dish_type")
public class DishType {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    long id;

    @Column(name = "title")
    String title;

    @JsonIgnore
    @ManyToMany
    @JoinTable(name = "dish_type_relship",
            joinColumns = @JoinColumn(name = "type_fk"),
            inverseJoinColumns = @JoinColumn(name = "dish_fk"))
    List<Dish> dishes;

}

