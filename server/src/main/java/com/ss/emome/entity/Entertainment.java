package com.ss.emome.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Myroslav on 28.12.2017.
 */
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "entertainment")
public class
Entertainment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    long id;

    @Column(name = "title")
    String title;

    @JsonIgnore
    @Column(name = "url")
    String url;

    @JsonIgnore
    @Column(name = "description")
    String description;

    @Column(name = "google_id")
    String googleId;

    @ManyToOne
    @JoinColumn(name = "coordinates_fk")
    Coordinates coordinates;

    @JsonIgnore
    @OneToMany(mappedBy = "entertainment")
    List<Dish> dishes;

    @ManyToMany
    @JoinTable(name = "entertainment_type_relship",
               joinColumns = @JoinColumn(name = "entertainment_fk"),
               inverseJoinColumns = @JoinColumn(name = "entertainment_type_fk"))
    List<EntertainmentType> entertainmentTypes;

    @ManyToMany
    @JoinTable(name = "entertainment_cuisine_relship",
               joinColumns = @JoinColumn(name = "entertainment_fk"),
               inverseJoinColumns = @JoinColumn(name = "cuisine_fk"))
    List<Cuisine> cuisines;

    @JsonIgnore
    @OneToMany(mappedBy = "entertainment")
    List<Order> orders;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "image_fk")
    Image image;

}
