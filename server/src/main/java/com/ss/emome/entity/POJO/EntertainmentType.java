package com.ss.emome.entity.POJO;

import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * Created by Myroslav on 23.01.2018.
 */
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
@AllArgsConstructor
public class EntertainmentType {

    String title;

}
