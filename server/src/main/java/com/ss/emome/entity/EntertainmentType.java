package com.ss.emome.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Myroslav on 28.12.2017.
 */
@FieldDefaults(level = AccessLevel.PRIVATE)
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "entertainment_type")
public class EntertainmentType {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    long id;

    @Column(name = "title")
    String title;

    @JsonIgnore
    @ManyToMany
    @JoinTable(name = "entertainment_type_relship",
            joinColumns = @JoinColumn(name = "entertainment_type_fk"),
            inverseJoinColumns = @JoinColumn(name = "entertainment_fk"))
    List<Entertainment> entertainments;

}
