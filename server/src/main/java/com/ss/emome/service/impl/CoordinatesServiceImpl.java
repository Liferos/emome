package com.ss.emome.service.impl;

import com.ss.emome.entity.Coordinates;
import com.ss.emome.entity.Entertainment;
import com.ss.emome.exception.AlterableObjectNotExistsException;
import com.ss.emome.exception.ObjectIsNotRightException;
import com.ss.emome.exception.ObjectListIsEmptyException;
import com.ss.emome.exception.ObjectNotFoundException;
import com.ss.emome.repository.CoordinatesRepository;
import com.ss.emome.repository.EntertainmentRepository;
import com.ss.emome.service.CoordinatesService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Myroslav on 02.01.2018.
 */
@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public class CoordinatesServiceImpl implements CoordinatesService {

    final CoordinatesRepository coordinatesRepository;
    final EntertainmentRepository entertainmentRepository;

    @Override
    public Coordinates save(Coordinates coordinates) {
        if(coordinates == null) throw new ObjectIsNotRightException(Coordinates.class);
        return coordinatesRepository.saveAndFlush(coordinates);
    }

    @Override
    public Coordinates findOne(long id) {
        if(!coordinatesRepository.exists(id)) throw new ObjectNotFoundException(Coordinates.class, id);
        return coordinatesRepository.findOne(id);
    }

    @Override
    public List<Coordinates> findAll() {
        if(coordinatesRepository.isEmptyTable()) throw new ObjectListIsEmptyException(Coordinates.class);
        return coordinatesRepository.findAll();
    }

    @Override
    public void delete(long id) {
        if(!coordinatesRepository.exists(id)) throw new ObjectNotFoundException(Coordinates.class, id);
        coordinatesRepository.delete(id);
    }

    @Override
    public void update(Coordinates alteredCoordinates) {
        if(alteredCoordinates == null) throw new ObjectIsNotRightException(Coordinates.class);
        if(!coordinatesRepository.exists(alteredCoordinates.getId()))
            throw new AlterableObjectNotExistsException(Coordinates.class, alteredCoordinates.getId());
        coordinatesRepository.save(alteredCoordinates);
    }

    @Override
    public Coordinates findCoordinatesByEntertainment(long entertainmentId) {
        if (!entertainmentRepository.exists(entertainmentId)) throw new ObjectNotFoundException(Entertainment.class,
                                                                                            entertainmentId);
        return coordinatesRepository.findByEntertainmentId(entertainmentId);
    }

}
