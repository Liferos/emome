package com.ss.emome.service;

import com.ss.emome.entity.EntertainmentType;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * Created by Myroslav on 28.12.2017.
 */
public interface EntertainmentTypeService {

    EntertainmentType save(EntertainmentType entertainmentType);

    EntertainmentType findOne(long id);

    List<EntertainmentType> findAll();

    void delete(long id);

    void update(EntertainmentType alteredEntertainmentType);

    List<EntertainmentType> findEntertainmentTypeByEntertainment(long entertainmentId);

    EntertainmentType findEntertainmentTypeByTitle(String title);

}
