package com.ss.emome.service.impl;

import com.ss.emome.entity.Dish;
import com.ss.emome.entity.Entertainment;
import com.ss.emome.exception.ObjectListIsEmptyException;
import com.ss.emome.parseTool.GoogleResponseParser;
import com.ss.emome.service.DishService;
import com.ss.emome.service.EntertainmentService;
import com.ss.emome.service.GoogleMapService;
import com.ss.emome.util.FilterUtil;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Myroslav on 23.01.2018.
 */
@FieldDefaults(level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
@Service
public class GoogleMapServiceImpl implements GoogleMapService {

    private final String key = "AIzaSyBhwB7XZ5iPrW5Nkbu34E1xesRPXoUd0Lc";
    private final String url = "https://maps.googleapis.com/maps/api/place/search/json";

    final RestTemplate restTemplate;
    final GoogleResponseParser googleResponseParser;
    final EntertainmentService entertainmentService;
    final DishService dishService;
    final FilterUtil filterUtil;

    @Override
    public List<Entertainment> searchNearbyPlaces(double latitude,
                                                  double longitude,
                                                  int radius) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url)
                .queryParam("key", key)
                .queryParam("location", latitude + "," + longitude)
                .queryParam("radius", String.valueOf(radius))
                .queryParam("type", "cafe")
                .queryParam("language", "en");

        String response = restTemplate.getForObject(builder.buildAndExpand().toUri(), String.class);
        if(response.isEmpty()) throw new ObjectListIsEmptyException(Entertainment.class);

        return entertainmentService.checkAndSaveEntertainment(googleResponseParser.parseResponse(response));
    }

    @Override
    public List<Dish> searchDishListOfNearbyEntertainment(List<Entertainment> entertainmentList,
                                                          String name,
                                                          String cuisine,
                                                          String type) {
        List<Dish> result = new ArrayList<>();

        entertainmentList.forEach(e -> result.addAll(dishService.findDishByEntertainment(e.getId())));

        return filterUtil.filterDishListByCuisine(
                filterUtil.filterDishListByEntertainmentType(
                    filterUtil.filterDishListByName(result, name), type), cuisine
        );
    }

}