package com.ss.emome.service.impl;

import com.ss.emome.entity.Dish;
import com.ss.emome.entity.DishType;
import com.ss.emome.exception.AlterableObjectNotExistsException;
import com.ss.emome.exception.ObjectIsNotRightException;
import com.ss.emome.exception.ObjectListIsEmptyException;
import com.ss.emome.exception.ObjectNotFoundException;
import com.ss.emome.repository.DishRepository;
import com.ss.emome.repository.DishTypeRepository;
import com.ss.emome.service.DishTypeService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Myroslav on 28.12.2017.
 */
@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public class DishTypeServiceImpl implements DishTypeService {

    final DishTypeRepository dishTypeRepository;
    final DishRepository dishRepository;

    @Override
    public DishType save(DishType dishType) {
        if(dishType == null) throw new ObjectIsNotRightException(DishType.class);
        return dishTypeRepository.saveAndFlush(dishType);
    }

    @Override
    public DishType findOne(long id) {
        if(!dishTypeRepository.exists(id)) throw new ObjectNotFoundException(DishType.class, id);
        return dishTypeRepository.findOne(id);
    }

    @Override
    public List<DishType> findAll() {
        if(dishTypeRepository.isEmptyTable()) throw new ObjectListIsEmptyException(DishType.class);
        return dishTypeRepository.findAll();
    }

    @Override
    public void delete(long id) {
        if(!dishTypeRepository.exists(id)) throw new ObjectNotFoundException(DishType.class, id);
        dishTypeRepository.delete(id);
    }

    @Override
    public void update(DishType dishType) {
        if(dishType == null) throw new ObjectIsNotRightException(DishType.class);
        if(!dishTypeRepository.exists(dishType.getId()))
            throw new AlterableObjectNotExistsException(DishType.class, dishType.getId());
        dishTypeRepository.save(dishType);
    }

    @Override
    public List<DishType> findDishTypeByDish(long dishId) {
        if(!dishRepository.exists(dishId)) throw new ObjectNotFoundException(Dish.class, dishId);
        return dishTypeRepository.findByDish(dishId);
    }

    @Override
    public DishType findDishTypeByTitle(String title) {
        return dishTypeRepository.findDishTypeByTitle(title);
    }

}
