package com.ss.emome.service.impl;

import com.ss.emome.entity.Dish;
import com.ss.emome.entity.DishType;
import com.ss.emome.entity.Entertainment;
import com.ss.emome.entity.Image;
import com.ss.emome.exception.*;
import com.ss.emome.repository.DishRepository;
import com.ss.emome.repository.DishTypeRepository;
import com.ss.emome.repository.EntertainmentRepository;
import com.ss.emome.repository.ImageRepository;
import com.ss.emome.service.DishService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Myroslav on 28.12.2017.
 */
@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public class DishServiceImpl implements DishService {

    final DishRepository dishRepository;
    final EntertainmentRepository entertainmentRepository;
    final DishTypeRepository dishTypeRepository;
    final ImageRepository imageRepository;

    @Override
    public Dish save(Dish dish) {
        if(dish == null) throw new ObjectIsNotRightException(Dish.class);
        return dishRepository.saveAndFlush(dish);
    }

    @Override
    public Dish findOne(long id) {
        if(!dishRepository.exists(id)) throw new ObjectNotFoundException(Dish.class, id);
        return dishRepository.findOne(id);
    }

    @Override
    public List<Dish> findAll() {
        if(dishRepository.isEmptyTable()) throw new ObjectListIsEmptyException(Dish.class);
        return dishRepository.findAll();
    }

    @Override
    public void delete(long id) {
        if(!dishRepository.exists(id)) throw new ObjectNotFoundException(Dish.class, id);
        dishRepository.delete(id);
    }

    @Override
    public void update(Dish dish) {
        if(dish == null) throw new ObjectIsNotRightException(Dish.class);
        if(!dishRepository.exists(dish.getId())) throw new AlterableObjectNotExistsException(Dish.class, dish.getId());
        dishRepository.save(dish);
    }

    @Override
    public boolean dishTypeRelationshipExists(long dishId, long dishTypeId) {
        return dishRepository.dishTypeRelationshipExists(dishId, dishTypeId);
    }

    @Override
    public void bindDishToEntertainment(long dishId, long entertainmentId) {
        if(!dishRepository.exists(dishId)) throw new ObjectNotFoundException(Dish.class, dishId);
        if(!entertainmentRepository.exists(entertainmentId))
            throw new ObjectNotFoundException(Entertainment.class, entertainmentId);
        dishRepository.bindEntertainment(dishId, entertainmentId);
    }

    @Override
    public void bindDishToDishType(long dishId, long dishTypeId) {
        if(!dishRepository.exists(dishId)) throw new ObjectNotFoundException(Dish.class, dishId);
        if(!dishTypeRepository.exists(dishTypeId)) throw new ObjectNotFoundException(DishType.class, dishTypeId);
        if(dishTypeRelationshipExists(dishId, dishTypeId))
            throw new BindExistsException(Dish.class, DishType.class, dishId, dishTypeId);
        dishRepository.bindDishType(dishId, dishTypeId);
    }

    @Override
    public List<Dish> findDishByEntertainment(long id) {
        if(!entertainmentRepository.exists(id)) throw new ObjectNotFoundException(Entertainment.class, id);
        return dishRepository.findByEntertainment(id);
    }

    @Override
    public List<Dish> findDishByDishType(long id) {
        if(!dishTypeRepository.exists(id)) throw new ObjectNotFoundException(DishType.class, id);
        return dishRepository.findByDishType(id);
    }

    @Override
    public void bindImage(long dishId, long imageId) {
        if(!dishRepository.exists(dishId)) throw new ObjectNotFoundException(Dish.class, dishId);
        if(!imageRepository.exists(imageId)) throw new ObjectNotFoundException(Image.class, imageId);
        dishRepository.bindImage(dishId, imageId);
    }

    @Override
    public Dish updateEmo(long id, double emo) {
        if(!dishRepository.exists(id)) throw new ObjectNotFoundException(Dish.class, id);
        Dish tmpDish = dishRepository.findOne(id);
        tmpDish.setEmoComponent((tmpDish.getEmoComponent() + emo)/2);
        tmpDish.setEmoNum(tmpDish.getEmoNum() + 1);
        return dishRepository.save(tmpDish);
    }

}
