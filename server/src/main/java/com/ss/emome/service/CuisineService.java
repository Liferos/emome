package com.ss.emome.service;

import com.ss.emome.entity.Cuisine;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * Created by Myroslav on 28.12.2017.
 */
public interface CuisineService {

    Cuisine save(Cuisine cuisine);

    Cuisine findOne(long id);

    List<Cuisine> findAll();

    void delete(long id);

    void update(Cuisine cuisine);

    List<Cuisine> findCuisineByEntertainment(long entertainmentId);

}
