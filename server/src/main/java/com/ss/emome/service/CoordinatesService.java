package com.ss.emome.service;

import com.ss.emome.entity.Coordinates;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * Created by Myroslav on 02.01.2018.
 */
public interface CoordinatesService {

    Coordinates save(Coordinates coordinates);

    Coordinates findOne(long id);

    List<Coordinates> findAll();

    void delete(long id);

    void update(Coordinates alteredCoordinates);

    Coordinates findCoordinatesByEntertainment(long entertainmentId);

}
