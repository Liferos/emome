package com.ss.emome.service.impl;

import com.ss.emome.customData.CustomDataGenerator;
import com.ss.emome.entity.*;
import com.ss.emome.exception.*;
import com.ss.emome.repository.*;
import com.ss.emome.service.EntertainmentService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Myroslav on 28.12.2017.
 */
@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public class EntertainmentServiceImpl implements EntertainmentService {

    final EntertainmentRepository entertainmentRepository;
    final EntertainmentTypeRepository entertainmentTypeRepository;
    final CuisineRepository cuisineRepository;
    final CoordinatesRepository coordinatesRepository;
    final ImageRepository imageRepository;
    final CustomDataGenerator customDataGenerator;
    final DishRepository dishRepository;

    @Override
    public Entertainment save(Entertainment entertainment) {
        if(entertainment == null) throw new ObjectIsNotRightException(Entertainment.class);
        return entertainmentRepository.saveAndFlush(entertainment);
    }

    @Override
    public Entertainment findOne(long id) {
        if(!entertainmentRepository.exists(id)) throw new ObjectNotFoundException(Entertainment.class, id);
        return entertainmentRepository.findOne(id);
    }

    @Override
    public List<Entertainment> findAll() {
        if(entertainmentRepository.isEmptyTable()) throw new ObjectListIsEmptyException(Entertainment.class);
        return entertainmentRepository.findAll();
    }

    @Override
    public void delete(long id) {
        if(!entertainmentRepository.exists(id)) throw new ObjectNotFoundException(Entertainment.class, id);
        entertainmentRepository.delete(id);
    }

    @Override
    public boolean entertainmentCuisineRelationshipExists(long entertainmentId, long cuisineId) {
        return entertainmentRepository.entertainmentCuisineRelationshipExists(entertainmentId, cuisineId);
    }

    @Override
    public boolean entertainmentEntertainmentTypeRelationshipExists(long entertainmentId, long entertainmentTypeId) {
        return entertainmentRepository.entertainmentTypeRelationshipExists(entertainmentId, entertainmentTypeId);
    }

    @Override
    public void update(Entertainment alteredEntertainment) {
        if(alteredEntertainment == null) throw new ObjectIsNotRightException(Entertainment.class);
        if(!entertainmentRepository.exists(alteredEntertainment.getId()))
            throw new AlterableObjectNotExistsException(Entertainment.class,alteredEntertainment.getId());
        entertainmentRepository.save(alteredEntertainment);
    }

    @Override
    public void bindEntertainmentToCuisine(long entertainmentId, long cuisineId) {
        if(!entertainmentRepository.exists(entertainmentId)) {
            throw new ObjectNotFoundException(Entertainment.class, entertainmentId);}
        if(!cuisineRepository.exists(cuisineId)) {
            throw new ObjectNotFoundException(Cuisine.class, cuisineId);}
        if(entertainmentCuisineRelationshipExists(entertainmentId, cuisineId))
            throw new BindExistsException(Entertainment.class, Cuisine.class, entertainmentId, cuisineId);

        entertainmentRepository.bindCuisine(entertainmentId, cuisineId);
    }

    @Override
    public void bindEntertainmentToEntertainmentType(long entertainmentId, long entertainmentTypeId) {
        if(!entertainmentRepository.exists(entertainmentId))
            throw new ObjectNotFoundException(Entertainment.class, entertainmentId);
        if(!entertainmentTypeRepository.exists(entertainmentTypeId))
            throw new ObjectNotFoundException(EntertainmentType.class, entertainmentTypeId);
        if(entertainmentEntertainmentTypeRelationshipExists(entertainmentId, entertainmentTypeId))
            throw new BindExistsException(Entertainment.class, EntertainmentType.class,
                                          entertainmentId, entertainmentTypeId);

        entertainmentRepository.bindType(entertainmentId, entertainmentTypeId);
    }

    @Override
    public void bindEntertainmentToCoordinates(long entertainmentId, long coordinatesId) {
        if(!entertainmentRepository.exists(entertainmentId))
            throw new ObjectNotFoundException(Entertainment.class, entertainmentId);
        if(!coordinatesRepository.exists(coordinatesId))
            throw new ObjectNotFoundException(Coordinates.class, coordinatesId);

        entertainmentRepository.bindCoordinates(entertainmentId, coordinatesId);
    }

    @Override
    public List<Entertainment> findEntertainmentByEntertainmentType(long id) {
        List<Entertainment> tmpEntertainmentList = entertainmentRepository.findByEntertainmentTypeId(id);
        if(tmpEntertainmentList.isEmpty()) throw new ObjectListIsEmptyException(Entertainment.class);
        return tmpEntertainmentList;
    }

    @Override
    public List<Entertainment> findEntertainmentByCuisine(long id) {
        List<Entertainment> tmpEntertainmentList = entertainmentRepository.findByCuisineId(id);
        if(tmpEntertainmentList.isEmpty()) throw new ObjectListIsEmptyException(Entertainment.class);
        return tmpEntertainmentList;
    }

    @Override
    public List<Entertainment> findEntertainmentByCoordinates(long id) {
        List<Entertainment> tmpEntertainmentList = entertainmentRepository.findByCoordinatesId(id);
        if(tmpEntertainmentList.isEmpty()) throw new ObjectListIsEmptyException(Entertainment.class);
        return tmpEntertainmentList;
    }

    @Override
    public void bindImage(long entertainmentId, long imageId) {
        if(!entertainmentRepository.exists(entertainmentId))
            throw new ObjectNotFoundException(Entertainment.class, entertainmentId);
        if(!imageRepository.exists(imageId)) throw new ObjectNotFoundException(Image.class, imageId);
        entertainmentRepository.bindImage(entertainmentId, imageId);
    }

    @Override
    public boolean existsByGooglePlaceId(String placeId) {
        return entertainmentRepository.existsByGooglePlaceId(placeId);
    }

    @Override
    public List<Entertainment> checkAndSaveEntertainment(List<Entertainment> entertainmentList) {
        List<Entertainment> result = new ArrayList<>();
        entertainmentList.forEach(t -> {
            if(!existsByGooglePlaceId(t.getGoogleId())) {
                Coordinates tmpCoords = t.getCoordinates();
                t.setCoordinates(null);
                Entertainment tmp = save(t);

                bindEntertainmentToCoordinates(tmp.getId(), coordinatesRepository.save(tmpCoords).getId());
                customDataGenerator.bindRandomEntertainmentType(tmp);
                customDataGenerator.bindRandomCuisine(tmp);
                customDataGenerator.bindCustomDishList(
                        customDataGenerator.getCustomDishListForEntertainment(findOne(tmp.getId())));
                customDataGenerator.bindCustomCommentList(dishRepository.findByEntertainment(tmp.getId()));
                result.add(findOne(tmp.getId()));
            } else {
                result.add(findEntertainmentByPlaceId(t.getGoogleId()));
            }
        });
        return result;
    }

    public Entertainment findEntertainmentByPlaceId(String placeId) {
        if(!entertainmentRepository.existsByGooglePlaceId(placeId))
            throw new ObjectNotFoundException(Entertainment.class, 0);
        return entertainmentRepository.findByPlaceId(placeId);
    }

}
