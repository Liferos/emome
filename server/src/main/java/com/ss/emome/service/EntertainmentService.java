package com.ss.emome.service;

import com.ss.emome.entity.Entertainment;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * Created by Myroslav on 28.12.2017.
 */
public interface EntertainmentService {

    Entertainment save(Entertainment entertainment);

    Entertainment findOne(long id);

    List<Entertainment> findAll();

    void delete(long id);

    boolean entertainmentCuisineRelationshipExists(long entertainmentId, long cuisineId);

    boolean entertainmentEntertainmentTypeRelationshipExists(long entertainmentId, long entertainmentTypeId);

    void update(Entertainment alteredEntertainment);

    void bindEntertainmentToCuisine(long entertainmentId, long cuisineId);

    void bindEntertainmentToEntertainmentType(long entertainmentId, long entertainmentTypeId);

    void bindEntertainmentToCoordinates(long entertainmentId, long coordinatesId);

    List<Entertainment> findEntertainmentByEntertainmentType(long id);

    List<Entertainment> findEntertainmentByCuisine(long id);

    List<Entertainment> findEntertainmentByCoordinates(long id);

    void bindImage(long entertainmentId, long imageId);

    boolean existsByGooglePlaceId(String placeId);

    List<Entertainment> checkAndSaveEntertainment(List<Entertainment> entertainmentList);

    Entertainment findEntertainmentByPlaceId(String placeId);

}
