package com.ss.emome.service;

import com.ss.emome.entity.Image;

import java.util.List;

/**
 * Created by Myroslav on 18.01.2018.
 */
public interface ImageService {

    Image findOne(long id);

    List<Image> findAll();

    Image createImage(Image image);

    Image findByEntertainment(long id);

    Image findByDish(long id);

}
