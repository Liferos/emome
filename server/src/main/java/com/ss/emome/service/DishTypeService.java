package com.ss.emome.service;

import com.ss.emome.entity.DishType;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * Created by Myroslav on 28.12.2017.
 */
public interface DishTypeService {

    DishType save(DishType DishType);

    DishType findOne(long id);

    List<DishType> findAll();

    void delete(long id);

    void update(DishType dishType);

    List<DishType> findDishTypeByDish(long dishId);

    DishType findDishTypeByTitle(String title);

}
