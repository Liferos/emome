package com.ss.emome.service;

import com.ss.emome.entity.Order;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Myroslav on 09.01.2018.
 */
public interface OrderService {

    Order createOrder(Timestamp date, long entertainmentId);

    Order findOne(long id);

    List<Order> findAll();

    void delete(long id);

    List<Order> findOrderByEntertainment(long entertainmentId);

    Order updateOrder(Order alterableOrder);

}
