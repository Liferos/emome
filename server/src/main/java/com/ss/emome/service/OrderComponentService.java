package com.ss.emome.service;

import com.ss.emome.entity.OrderComponent;

import java.util.List;

/**
 * Created by Myroslav on 25.01.2018.
 */
public interface OrderComponentService {

    List<OrderComponent> createOrderComponentList(String orderComponentListJson, long orderId);

}
