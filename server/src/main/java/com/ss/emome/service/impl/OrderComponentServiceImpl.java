package com.ss.emome.service.impl;

import com.ss.emome.entity.OrderComponent;
import com.ss.emome.entityConverter.OrderComponentPojoConverter;
import com.ss.emome.parseTool.OrderComponentParser;
import com.ss.emome.repository.OrderComponentRepository;
import com.ss.emome.service.OrderComponentService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Myroslav on 25.01.2018.
 */
@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public class OrderComponentServiceImpl implements OrderComponentService {

    final OrderComponentRepository orderComponentRepository;
    final OrderComponentParser orderComponentParser;
    final OrderComponentPojoConverter orderComponentPojoConverter;

    @Override
    public List<OrderComponent> createOrderComponentList(String orderComponentListJson, long orderId) {
        List<OrderComponent> result = orderComponentPojoConverter
                .fromOrderComponentPojo(orderComponentParser.parseOrderComponent(orderComponentListJson), orderId);
        result.forEach(orderComponentRepository::saveAndFlush);
        return result;
    }

}
