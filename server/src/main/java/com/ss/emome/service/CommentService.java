package com.ss.emome.service;


import com.ss.emome.entity.Comment;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * Created by Myroslav on 28.12.2017.
 */
public interface CommentService {

    Comment save(long id, Comment comment);

    Comment findOne(long id);

    List<Comment> findAll();

    List<Comment> findCommentsByDish(long dishId);


    void delete(long id);

    void bindCommentToDish(long commentId, long dishId);

    void update(Comment alteredComment);

}
