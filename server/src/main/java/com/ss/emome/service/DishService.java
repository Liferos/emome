package com.ss.emome.service;

import com.ss.emome.entity.Dish;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * Created by Myroslav on 28.12.2017.
 */
public interface DishService {

    Dish save(Dish dish);

    Dish findOne(long id);

    List<Dish> findAll();

    void delete(long id);

    void update(Dish dish);

    boolean dishTypeRelationshipExists(long dishId, long dishTypeId);

    void bindDishToEntertainment(long dishId, long entertainmentId);

    void bindDishToDishType(long dishId, long dishTypeId);

    List<Dish> findDishByEntertainment(long id);

    List<Dish> findDishByDishType(long id);

    void bindImage(long dishId, long imageId);

    Dish updateEmo(long id, double emo);

}
