package com.ss.emome.service.impl;

import com.ss.emome.entity.Entertainment;
import com.ss.emome.entity.Order;
import com.ss.emome.exception.AlterableObjectNotExistsException;
import com.ss.emome.exception.ObjectIsNotRightException;
import com.ss.emome.exception.ObjectListIsEmptyException;
import com.ss.emome.exception.ObjectNotFoundException;
import com.ss.emome.repository.EntertainmentRepository;
import com.ss.emome.repository.OrderRepository;
import com.ss.emome.service.OrderService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Myroslav on 09.01.2018.
 */
@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    final OrderRepository orderRepository;
    final EntertainmentRepository entertainmentRepository;

    @Override
    public Order createOrder(Timestamp date, long entertainmentId) {
        if(!entertainmentRepository.exists(entertainmentId))
            throw new ObjectNotFoundException(Entertainment.class, entertainmentId);
        return orderRepository.saveAndFlush(new Order(date, entertainmentRepository.findOne(entertainmentId)));
    }

    @Override
    public Order findOne(long id) {
        if(!orderRepository.exists(id)) throw new ObjectNotFoundException(Order.class, id);
        return orderRepository.findOne(id);
    }

    @Override
    public List<Order> findAll() {
        if(orderRepository.isEmptyTable()) throw new ObjectListIsEmptyException(Order.class);
        return orderRepository.findAll();
    }

    @Override
    public void delete(long id) {
        if(!orderRepository.exists(id)) throw new ObjectNotFoundException(Order.class, id);
        orderRepository.delete(id);
    }

    @Override
    public List<Order> findOrderByEntertainment(long entertainmentId) {
        if(!entertainmentRepository.exists(entertainmentId))
            throw new ObjectNotFoundException(Entertainment.class, entertainmentId);
        return orderRepository.findByEntertainment(entertainmentId);
    }

    @Override
    public Order updateOrder(Order alterableOrder) {
        if(alterableOrder == null) throw new ObjectIsNotRightException(Order.class);
        if(!orderRepository.exists(alterableOrder.getId()))
            throw new AlterableObjectNotExistsException(Order.class, alterableOrder.getId());
        return orderRepository.saveAndFlush(alterableOrder);
    }

}
