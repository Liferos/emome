package com.ss.emome.service;

import com.ss.emome.entity.Dish;
import com.ss.emome.entity.Entertainment;

import java.util.List;

/**
 * Created by Myroslav on 23.01.2018.
 */
public interface GoogleMapService {

    List<Entertainment> searchNearbyPlaces(double latitude, double longitude, int radius);

    List<Dish> searchDishListOfNearbyEntertainment(List<Entertainment> entertainmentList,
                                                   String name,
                                                   String cuisine,
                                                   String type);

}
