package com.ss.emome.service.impl;

import com.ss.emome.entity.Entertainment;
import com.ss.emome.entity.EntertainmentType;
import com.ss.emome.exception.AlterableObjectNotExistsException;
import com.ss.emome.exception.ObjectIsNotRightException;
import com.ss.emome.exception.ObjectListIsEmptyException;
import com.ss.emome.exception.ObjectNotFoundException;
import com.ss.emome.repository.EntertainmentRepository;
import com.ss.emome.repository.EntertainmentTypeRepository;
import com.ss.emome.service.EntertainmentTypeService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Myroslav on 28.12.2017.
 */
@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public class EntertainmentTypeServiceImpl implements EntertainmentTypeService {

    final EntertainmentTypeRepository entertainmentTypeRepository;
    final EntertainmentRepository entertainmentRepository;

    @Override
    public EntertainmentType save(EntertainmentType entertainmentType) {
        if(entertainmentType == null) throw new ObjectIsNotRightException(EntertainmentType.class);
        return entertainmentTypeRepository.saveAndFlush(entertainmentType);
    }

    @Override
    public EntertainmentType findOne(long id) {
        if(!entertainmentTypeRepository.exists(id)) throw new ObjectNotFoundException(EntertainmentType.class, id);
        return entertainmentTypeRepository.findOne(id);
    }

    @Override
    public List<EntertainmentType> findAll() {
        if(entertainmentTypeRepository.isEmptyTable()) throw new ObjectListIsEmptyException(EntertainmentType.class);
        return entertainmentTypeRepository.findAll();
    }

    @Override
    public void delete(long id) {
        if(!entertainmentTypeRepository.exists(id)) throw new ObjectNotFoundException(EntertainmentType.class, id);
        entertainmentTypeRepository.delete(id);
    }

    @Override
    public void update(EntertainmentType alteredEntertainmentType) {
        if(alteredEntertainmentType == null) throw new ObjectIsNotRightException(EntertainmentType.class);
        if(!entertainmentTypeRepository.exists(alteredEntertainmentType.getId()))
            throw new AlterableObjectNotExistsException(EntertainmentType.class, alteredEntertainmentType.getId());
        entertainmentTypeRepository.save(alteredEntertainmentType);
    }

    @Override
    public List<EntertainmentType> findEntertainmentTypeByEntertainment(long entertainmentId) {
        if(!entertainmentRepository.exists(entertainmentId)) throw new ObjectNotFoundException(Entertainment.class,
                                                                                               entertainmentId);
        return entertainmentTypeRepository.findByEntertainmentId(entertainmentId);
    }

    @Override
    public EntertainmentType findEntertainmentTypeByTitle(String title) {
        return entertainmentTypeRepository.findByTitle(title);
    }

}
