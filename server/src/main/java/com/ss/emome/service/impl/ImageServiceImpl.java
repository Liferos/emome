package com.ss.emome.service.impl;

import com.ss.emome.entity.Dish;
import com.ss.emome.entity.Entertainment;
import com.ss.emome.entity.Image;
import com.ss.emome.exception.ObjectIsNotRightException;
import com.ss.emome.exception.ObjectListIsEmptyException;
import com.ss.emome.exception.ObjectNotFoundException;
import com.ss.emome.repository.DishRepository;
import com.ss.emome.repository.EntertainmentRepository;
import com.ss.emome.repository.ImageRepository;
import com.ss.emome.service.ImageService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Myroslav on 18.01.2018.
 */
@FieldDefaults(level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
@Service
public class ImageServiceImpl implements ImageService {

    final ImageRepository imageRepository;
    final EntertainmentRepository entertainmentRepository;
    final DishRepository dishRepository;

    @Override
    public Image findOne(long id) {
        if(!imageRepository.exists(id)) throw new ObjectNotFoundException(Image.class, id);
        return imageRepository.findOne(id);
    }

    @Override
    public List<Image> findAll() {
        if(imageRepository.isEmptyTable()) throw new ObjectListIsEmptyException(Image.class);
        return imageRepository.findAll();
    }

    @Override
    public Image createImage(Image image) {
        if(image == null) throw new ObjectIsNotRightException(Image.class);
        return imageRepository.saveAndFlush(image);
    }

    @Override
    public Image findByEntertainment(long id) {
        if(!entertainmentRepository.exists(id)) throw new ObjectNotFoundException(Entertainment.class, id);
        return imageRepository.findByEntertainment(id);
    }

    @Override
    public Image findByDish(long id) {
        if(!dishRepository.exists(id)) throw new ObjectNotFoundException(Dish.class, id);
        return imageRepository.findByDish(id);
    }

}
