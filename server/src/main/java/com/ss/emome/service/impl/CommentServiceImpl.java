package com.ss.emome.service.impl;

import com.ss.emome.entity.Comment;
import com.ss.emome.entity.Dish;
import com.ss.emome.exception.AlterableObjectNotExistsException;
import com.ss.emome.exception.ObjectIsNotRightException;
import com.ss.emome.exception.ObjectListIsEmptyException;
import com.ss.emome.exception.ObjectNotFoundException;
import com.ss.emome.repository.CommentRepository;
import com.ss.emome.repository.DishRepository;
import com.ss.emome.service.CommentService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Myroslav on 28.12.2017.
 */
@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public class CommentServiceImpl implements CommentService {

    final CommentRepository commentRepository;
    final DishRepository dishRepository;

    @Override
    public Comment save(long id, Comment comment) {
        if(comment == null) throw new ObjectIsNotRightException(Comment.class);
        if(!dishRepository.exists(id)) throw new ObjectNotFoundException(Dish.class, id);
        Comment tmpComment = commentRepository.saveAndFlush(comment);
        bindCommentToDish(tmpComment.getId(), id);
        return tmpComment;
    }

    @Override
    public Comment findOne(long id) {
        if(!commentRepository.exists(id)) throw new ObjectNotFoundException(Comment.class, id);
        return commentRepository.findOne(id);
    }

    @Override
    public List<Comment> findAll() {
        if(commentRepository.isEmptyTable()) throw new ObjectListIsEmptyException(Comment.class);
        return commentRepository.findAll();
    }

    @Override
    public void delete(long id) {
        if(!commentRepository.exists(id)) throw new ObjectNotFoundException(Comment.class, id);
        commentRepository.delete(id);
    }

    @Override
    public void bindCommentToDish(long commentId, long dishId) {
        if(!commentRepository.exists(commentId)) throw new ObjectNotFoundException(Comment.class, commentId);
        if(!dishRepository.exists(dishId)) throw new ObjectNotFoundException(Dish.class, dishId);
        Dish tmpDish = dishRepository.findOne(dishId);
        tmpDish.setCommentNum(tmpDish.getCommentNum() + 1);
        dishRepository.save(tmpDish);
        commentRepository.bindDish(commentId, dishId);
    }

    @Override
    public void update(Comment alteredComment) {
        if(alteredComment == null) throw new ObjectIsNotRightException(Comment.class);
        if(!commentRepository.exists(alteredComment.getId()))
            throw new AlterableObjectNotExistsException(Comment.class, alteredComment.getId());
        commentRepository.save(alteredComment);
    }

    @Override
    public List<Comment> findCommentsByDish(long dishId) {
        if(!dishRepository.exists(dishId)) throw new ObjectNotFoundException(Dish.class, dishId);
        return commentRepository.findByDish(dishId);
    }

}