package com.ss.emome.service.impl;

import com.ss.emome.entity.Cuisine;
import com.ss.emome.entity.Entertainment;
import com.ss.emome.exception.AlterableObjectNotExistsException;
import com.ss.emome.exception.ObjectIsNotRightException;
import com.ss.emome.exception.ObjectListIsEmptyException;
import com.ss.emome.exception.ObjectNotFoundException;
import com.ss.emome.repository.CuisineRepository;
import com.ss.emome.repository.EntertainmentRepository;
import com.ss.emome.service.CuisineService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Myroslav on 28.12.2017.
 */
@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public class CuisineServiceImpl implements CuisineService {

    final CuisineRepository cuisineRepository;
    final EntertainmentRepository entertainmentRepository;

    @Override
    public Cuisine save(Cuisine cuisine) {
        if(cuisine == null) throw new ObjectIsNotRightException(Cuisine.class);
        return cuisineRepository.saveAndFlush(cuisine);
    }

    @Override
    public Cuisine findOne(long id) {
        if(!cuisineRepository.exists(id)) throw new ObjectNotFoundException(Cuisine.class, id);
        return cuisineRepository.findOne(id);
    }

    @Override
    public List<Cuisine> findAll() {
        if(cuisineRepository.isEmptyTable()) throw new ObjectListIsEmptyException(Cuisine.class);
        return cuisineRepository.findAll();
    }

    @Override
    public void delete(long id) {
        if(!cuisineRepository.exists(id)) throw new ObjectNotFoundException(Cuisine.class, id);
        cuisineRepository.delete(id);
    }

    @Override
    public void update(Cuisine alteredCuisine) {
        if(alteredCuisine == null) throw new ObjectIsNotRightException(Cuisine.class);
        if(!cuisineRepository.exists(alteredCuisine.getId()))
            throw new AlterableObjectNotExistsException(Cuisine.class, alteredCuisine.getId());
        cuisineRepository.save(alteredCuisine);
    }

    @Override
    public List<Cuisine> findCuisineByEntertainment(long entertainmentId) {
        if(!entertainmentRepository.exists(entertainmentId))
            throw new ObjectNotFoundException(Entertainment.class, entertainmentId);
        return cuisineRepository.findByEntertainmentId(entertainmentId);
    }

}
