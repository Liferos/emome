package com.ss.emome.exception;

/**
 * Created by Myroslav on 12.01.2018.
 */
public class BindExistsException extends RuntimeException {

    public BindExistsException(Class firstObject, Class secondObject, long firstId, long secondId) {
        super("Cannot bind these objects(" + firstObject.getSimpleName() + ":id=" + firstId + ", "
                                           + secondObject.getSimpleName() + ":id="+ secondId +") " +
              "because such a relationship already exists.");
    }

}
