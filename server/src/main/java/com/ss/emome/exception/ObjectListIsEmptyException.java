package com.ss.emome.exception;

/**
 * Created by Myroslav on 12.01.2018.
 */
public class ObjectListIsEmptyException extends RuntimeException {

    public ObjectListIsEmptyException(Class object) {
        super("Requested " + object.getSimpleName() + " list is empty.");
    }

}
