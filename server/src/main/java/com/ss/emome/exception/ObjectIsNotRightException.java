package com.ss.emome.exception;

/**
 * Created by Myroslav on 15.01.2018.
 */
public class ObjectIsNotRightException extends RuntimeException {

    public ObjectIsNotRightException(Class object) {
        super("This object " + object.getSimpleName() + " is not right.");
    }

}
