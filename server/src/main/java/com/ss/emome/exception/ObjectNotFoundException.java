package com.ss.emome.exception;

/**
 * Created by Myroslav on 12.01.2018.
 */
public class ObjectNotFoundException extends RuntimeException {

    public ObjectNotFoundException(Class object, long id) {
        super("There is no such "+ object.getSimpleName() +" with specified id='" + id +"'.");
    }

}