package com.ss.emome.exception;

/**
 * Created by Myroslav on 12.01.2018.
 */
public class AlterableObjectNotExistsException extends RuntimeException {

    public AlterableObjectNotExistsException(Class object, long id) {
        super("Alterable " + object.getSimpleName() + " with specified id '" + id + "' is not exists in the database.");
    }

}
