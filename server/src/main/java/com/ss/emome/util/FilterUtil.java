package com.ss.emome.util;

import com.ss.emome.entity.Dish;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Myroslav on 01.02.2018.
 */
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Component
public class FilterUtil {

    public List<Dish> filterDishListByCuisine(List<Dish> dishList, String cuisine) {
        if(cuisine == null) return dishList;
        return dishList.stream().filter((d) -> d.getEntertainment().getCuisines().stream()
                        .anyMatch(c -> c.getTitle().toLowerCase().trim().contains(cuisine.toLowerCase().trim())))
                .collect(Collectors.toList());
    }

    public List<Dish> filterDishListByEntertainmentType(List<Dish> dishList, String type) {
        if(type == null) return dishList;
        return dishList.stream().filter((d) -> d.getEntertainment().getEntertainmentTypes().stream()
                .anyMatch(e -> e.getTitle().toLowerCase().trim().contains(type.toLowerCase().trim())))
                .collect(Collectors.toList());
    }

    public List<Dish> filterDishListByName(List<Dish> dishList, String name) {
        if(name == null) return dishList;
        return dishList.stream()
                .filter((d) -> d.getTitle().toLowerCase().trim().contains(name.toLowerCase().trim()))
                .collect(Collectors.toList());
    }

}
