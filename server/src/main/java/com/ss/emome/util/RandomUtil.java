package com.ss.emome.util;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * Created by Myroslav on 01.02.2018.
 */
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Component
public class RandomUtil {

    Random r = new Random();

    public int getRandIntBySpecifiedRange(int left, int right) {
        return r.nextInt(right - left) + left;
    }

}
