package com.ss.emome.entityConverter;

import com.ss.emome.entity.OrderComponent;
import com.ss.emome.service.DishService;
import com.ss.emome.service.OrderService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Myroslav on 25.01.2018.
 */
@FieldDefaults(level = AccessLevel.PRIVATE)
@Component
@RequiredArgsConstructor
public class OrderComponentPojoConverter {

    final DishService dishService;
    final OrderService orderService;

    public List<OrderComponent> fromOrderComponentPojo(List<com.ss.emome.entity.POJO.OrderComponent> orderComponentList,
                                          long orderId) {
        List<OrderComponent> result = new ArrayList<>();
        orderComponentList.forEach(t -> {
            result.add(new OrderComponent(t.getAmount(),
                                          dishService.findOne(t.getDishId()),
                                          orderService.findOne(orderId)));
        });
        return result;
    }

}
