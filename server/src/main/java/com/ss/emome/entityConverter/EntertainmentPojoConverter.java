package com.ss.emome.entityConverter;

import com.ss.emome.entity.Coordinates;
import com.ss.emome.entity.Entertainment;
import com.ss.emome.entity.EntertainmentType;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Myroslav on 24.01.2018.
 */
@Component
public class EntertainmentPojoConverter {

    private EntertainmentPojoConverter() {}

    public static Entertainment fromEntertainmentPOJO(com.ss.emome.entity.POJO.Entertainment entertainment) {
        Entertainment result = new Entertainment();

        result.setTitle(entertainment.getName());
        result.setGoogleId(entertainment.getPlace_id());
        result.setCoordinates(new Coordinates());
        result.getCoordinates().setLatitude(Double.valueOf(entertainment.getLatitude()));
        result.getCoordinates().setLongitude(Double.valueOf(entertainment.getLongitude()));

        return result;
    }

    public static List<Entertainment> fromEntertainmentPOJOList(
            List<com.ss.emome.entity.POJO.Entertainment> entertainmentList) {
        List<Entertainment> result = new ArrayList<>();
        entertainmentList.forEach(t -> result.add(fromEntertainmentPOJO(t)));

        return result;
    }

}
