package com.ss.emome.repository;

import com.ss.emome.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Myroslav on 09.01.2018.
 */
@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    @Query(value = "select case when count(*) < 1 then 'true' else 'false' end from order_table", nativeQuery = true)
    boolean isEmptyTable();

    @Query(value = "select * from order_table where entertainment_fk = :entertainmentId", nativeQuery = true)
    List<Order> findByEntertainment(@Param("entertainmentId") long entertainmentId);

}
