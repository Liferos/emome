package com.ss.emome.repository;

import com.ss.emome.entity.Entertainment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Myroslav on 28.12.2017.
 */
@Repository
public interface EntertainmentRepository extends JpaRepository<Entertainment, Long>{

    @Query(value = "select * from entertainment where id in " +
            "(select entertainment_fk from entertainment_type_relship " +
            "where entertainment_type_fk = :id)", nativeQuery = true)
    List<Entertainment> findByEntertainmentTypeId(@Param("id") long id);

    @Query(value = "select * from entertainment where id in " +
            "(select entertainment_fk from entertainment_cuisine_relship " +
            "where cuisine_fk = :id)", nativeQuery = true)
    List<Entertainment> findByCuisineId(@Param("id") long id);

    @Query(value = "select * from entertainment where coordinates_fk = :id", nativeQuery = true)
    List<Entertainment> findByCoordinatesId(@Param("id") long id);

    @Query(value = "select case when count(*) > 0 then 'true' else 'false' end " +
            "from entertainment_type_relship where entertainment_fk = :entertainmentId and " +
            "entertainment_type_fk = :entertainmentTypeId", nativeQuery = true)
    boolean entertainmentTypeRelationshipExists(@Param("entertainmentId") long entertainmentId,
                                                @Param("entertainmentTypeId") long entertainmentTypeId);

    @Query(value = "select case when count(*) > 0 then 'true' else 'false' end " +
            "from entertainment_cuisine_relship where entertainment_fk = :entertainmentId and " +
            "cuisine_fk = :cuisineId", nativeQuery = true)
    boolean entertainmentCuisineRelationshipExists(@Param("entertainmentId") long entertainmentId,
                                                   @Param("cuisineId") long cuisineId);

    @Query(value = "select case when count(*) < 1 then 'true' else 'false' end from entertainment", nativeQuery = true)
    boolean isEmptyTable();

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "insert into entertainment_type_relship(entertainment_fk, entertainment_type_fk) " +
            "VALUES (:entertainmentId, :typeId)", nativeQuery = true)
    void bindType(@Param("entertainmentId") long entertainmentId,
                  @Param("typeId") long typeId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "insert into entertainment_cuisine_relship(entertainment_fk, cuisine_fk) " +
            "VALUES (:entertainmentId, :cuisineId)", nativeQuery = true)
    void bindCuisine(@Param("entertainmentId") long entertainmentId,
                     @Param("cuisineId") long cuisineId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "update entertainment set coordinates_fk = :coordinatesId where id = :entertainmentId",
            nativeQuery = true)
    void bindCoordinates(@Param("entertainmentId") long entertainmentId,
                         @Param("coordinatesId") long coordinatesId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "update entertainment set image_fk = :imageId where id = :entertainmentId", nativeQuery = true)
    void bindImage(@Param("entertainmentId") long entertainmentId, @Param("imageId") long imageId);

    @Query(value = "select case when count(*) > 0 then 'true' else 'false' end from entertainment " +
            "where (locate(:placeId, google_id) > 0)", nativeQuery = true)
    boolean existsByGooglePlaceId(@Param("placeId") String placeId);

    @Query(value = "select * from entertainment where (locate(:placeId, google_id) > 0)", nativeQuery = true)
    Entertainment findByPlaceId(@Param("placeId") String placeId);

}