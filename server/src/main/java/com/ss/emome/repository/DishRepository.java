package com.ss.emome.repository;

import com.ss.emome.entity.Dish;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Myroslav on 28.12.2017.
 */
@Repository
public interface DishRepository extends JpaRepository<Dish, Long> {

    @Query(value = "select case when count(*) < 1 then 'true' else 'false' end from dish", nativeQuery = true)
    boolean isEmptyTable();

    @Query(value = "select case when count(*) > 0 then 'true' else 'false' end from dish_type_relship " +
            "where dish_fk = :dishId and type_fk = :dishTypeId", nativeQuery = true)
    boolean dishTypeRelationshipExists(@Param("dishId") long dishId,
                                       @Param("dishTypeId") long dishTypeId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "update dish set entertainment_fk = :entertainmentId where id = :dishId", nativeQuery = true)
    void bindEntertainment(@Param("dishId") long dishId,
                           @Param("entertainmentId") long entertainmentId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "insert into dish_type_relship(dish_fk, type_fk) values(:dishId, :dishTypeId)", nativeQuery = true)
    void bindDishType(@Param("dishId") long dishId,
                      @Param("dishTypeId")long dishTypeId);

    @Query(value = "select * from dish where entertainment_fk = :entertainmentId", nativeQuery = true)
    List<Dish> findByEntertainment(@Param("entertainmentId") long entertainmentId);

    @Query(value = "select * from dish where id in(select dish_fk from dish_type_relship " +
            "where type_fk = :dishTypeId)", nativeQuery = true)
    List<Dish> findByDishType(@Param("dishTypeId") long dishTypeId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "update dish set image_fk = :imageId where id = :dishId", nativeQuery = true)
    void bindImage(@Param("dishId") long dishId, @Param("imageId") long imageId);

}