package com.ss.emome.repository;

import com.ss.emome.entity.Coordinates;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Myroslav on 02.01.2018.
 */
@Repository
public interface CoordinatesRepository extends JpaRepository<Coordinates, Long> {

    @Query(value = "select case when count(*) < 1 then 'true' else 'false' end from coordinates", nativeQuery = true)
    boolean isEmptyTable();

    @Query(value = "select * from coordinates where id in " +
            "(select coordinates_fk from entertainment where id = :entertainmentId)", nativeQuery = true)
    Coordinates findByEntertainmentId(@Param("entertainmentId") long entertainmentId);

}
