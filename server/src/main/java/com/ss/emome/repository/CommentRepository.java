package com.ss.emome.repository;

import com.ss.emome.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Myroslav on 28.12.2017.
 */
@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

    @Query(value = "select case when count(*) < 1 then 'true' else 'false' end from comments", nativeQuery = true)
    boolean isEmptyTable();

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "update comments set dish_fk = :dishId where id = :commentId", nativeQuery = true)
    void bindDish(@Param("commentId") long commentId,
                  @Param("dishId") long dishId);

    @Query(value = "select * from comments where dish_fk = :dishId", nativeQuery = true)
    List<Comment> findByDish(@Param("dishId") long dishId);

}
