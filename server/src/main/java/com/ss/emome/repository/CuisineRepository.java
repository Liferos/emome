package com.ss.emome.repository;

import com.ss.emome.entity.Cuisine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Myroslav on 28.12.2017.
 */
@Repository
public interface CuisineRepository  extends JpaRepository<Cuisine, Long> {

    @Query(value = "select case when count(*) < 1 then 'true' else 'false' end from cuisine", nativeQuery = true)
    boolean isEmptyTable();

    @Query(value = "select * from cuisine where id in " +
            "(select cuisine_fk from entertainment_cuisine_relship " +
            "where entertainment_fk = :entertainmentId)", nativeQuery = true)
    List<Cuisine> findByEntertainmentId(@Param("entertainmentId") long entertainmentId);



}
