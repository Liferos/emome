package com.ss.emome.repository;

import com.ss.emome.entity.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by Myroslav on 18.01.2018.
 */
@Repository
public interface ImageRepository extends JpaRepository<Image, Long> {

    @Query(value = "select case when count(*) < 1 then 'true' else 'false' end from image", nativeQuery = true)
    boolean isEmptyTable();

    @Query(value = "select * from image where id in (select image_fk from entertainment where id = :id)",
            nativeQuery = true)
    Image findByEntertainment(@Param("id") long id);

    @Query(value = "select * from image where id in (select image_fk from dish where id = :id)",
            nativeQuery = true)
    Image findByDish(@Param("id") long id);

}
