package com.ss.emome.repository;

import com.ss.emome.entity.OrderComponent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Myroslav on 25.01.2018.
 */
@Repository
public interface OrderComponentRepository extends JpaRepository<OrderComponent, Long> {
}
