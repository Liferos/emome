package com.ss.emome.repository;

import com.ss.emome.entity.EntertainmentType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Myroslav on 28.12.2017.
 */
@Repository
public interface EntertainmentTypeRepository extends JpaRepository<EntertainmentType, Long> {

    @Query(value = "select case when count(*) < 1 then 'true' else 'false' end from entertainment_type", nativeQuery = true)
    boolean isEmptyTable();

    @Query(value = "select * from entertainment_type where id in " +
            "(select entertainment_type_fk from entertainment_type_relship " +
            "where entertainment_fk = :id)", nativeQuery = true)
    List<EntertainmentType> findByEntertainmentId(@Param("id") long id);

    @Query(value = "select * from entertainment_type where title = :typeTitle", nativeQuery = true)
    EntertainmentType findByTitle(@Param("typeTitle") String title);

}
