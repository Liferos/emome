package com.ss.emome.repository;

import com.ss.emome.entity.DishType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Myroslav on 28.12.2017.
 */
@Repository
public interface DishTypeRepository extends JpaRepository<DishType, Long> {

    @Query(value = "select case when count(*) < 1 then 'true' else 'false' end from dish_type", nativeQuery = true)
    boolean isEmptyTable();

    @Query(value = "select * from dish_type where id in (select type_fk from dish_type_relship " +
            "where dish_fk = :dishId)", nativeQuery = true)
    List<DishType> findByDish(@Param("dishId") long dishId);

    @Query(value = "select * from dish_type where title = :typeTitle", nativeQuery = true)
    DishType findDishTypeByTitle(@Param("typeTitle") String title);

}