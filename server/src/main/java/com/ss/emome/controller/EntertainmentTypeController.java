package com.ss.emome.controller;

import com.ss.emome.entity.EntertainmentType;
import com.ss.emome.service.EntertainmentTypeService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Myroslav on 02.01.2018.
 */
@FieldDefaults(level = AccessLevel.PRIVATE)
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "entertainmenttype")
public class EntertainmentTypeController {

    final EntertainmentTypeService entertainmentTypeService;

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getEntertainmentType(@PathVariable long id) {
        return ResponseEntity.status(HttpStatus.OK).body(entertainmentTypeService.findOne(id));
    }

    @GetMapping
    public ResponseEntity<?> getEntertainmentTypeList() {
        return ResponseEntity.status(HttpStatus.OK).body(entertainmentTypeService.findAll());
    }

    @PostMapping
    public ResponseEntity<?> createEntertainmentType(@RequestBody EntertainmentType entertainmentType) {
        return ResponseEntity.status(HttpStatus.CREATED).body(entertainmentTypeService.save(entertainmentType));
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> deleteEntertainmentType(@PathVariable long id) {
        entertainmentTypeService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping(value = "/byentertainment")
    public ResponseEntity<?> getEntertainmentTypeByEntertainment(
                                        @RequestParam long id) {
        return ResponseEntity.status(HttpStatus.OK).body(entertainmentTypeService
                                                            .findEntertainmentTypeByEntertainment(id));
    }

    @PutMapping
    public ResponseEntity<?> updateEntertainmentType(@RequestBody EntertainmentType alteredEntertainmentType) {
        entertainmentTypeService.update(alteredEntertainmentType);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

}
