package com.ss.emome.controller;

import com.ss.emome.entity.Dish;
import com.ss.emome.service.DishService;
import com.ss.emome.service.GoogleMapService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Myroslav on 02.01.2018.
 */
@FieldDefaults(level = AccessLevel.PRIVATE)
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "dish")
public class DishController {

    final DishService dishService;
    final GoogleMapService googleMapService;

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getDish(@PathVariable long id) {
        return ResponseEntity.status(HttpStatus.OK).body(dishService.findOne(id));
    }

    @GetMapping
    public ResponseEntity<?> getDishList() {
        return ResponseEntity.status(HttpStatus.OK).body(dishService.findAll());
    }

    @PostMapping
    public ResponseEntity<?> createDish(@RequestBody Dish dish) {
        return ResponseEntity.status(HttpStatus.CREATED).body(dishService.save(dish));
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> deleteDish(@PathVariable long id) {
        dishService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping(value = "/byentertainment")
    public ResponseEntity<?> getDishByEntertainment(@RequestParam long id) {
        return ResponseEntity.status(HttpStatus.OK).body(dishService.findDishByEntertainment(id));
    }

    @GetMapping(value = "/bydishtype")
    public ResponseEntity<?> getDishByDishType(@RequestParam long id) {
        return ResponseEntity.status(HttpStatus.OK).body(dishService.findDishByDishType(id));
    }

    @PostMapping(value = "/{dishid}/bindentertainment")
    public ResponseEntity<?> bindDishToEntertainment(@PathVariable(value = "dishid") long dishId,
                                                     @RequestParam long id) {
        dishService.bindDishToEntertainment(dishId, id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PostMapping(value = "/{dishid}/binddishtype")
    public ResponseEntity<?> bindDishToDishType(@PathVariable(value = "dishid") long dishId,
                                                @RequestParam long id) {
        dishService.bindDishToDishType(dishId, id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PutMapping
    public ResponseEntity<?> updateDish(@RequestBody Dish alteredDish) {
        dishService.update(alteredDish);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PostMapping(value = "/{dishid}/bindimage")
    public ResponseEntity<?> bindDishToImage(@PathVariable(value = "dishid") long dishId,
                                                      @RequestParam long id) {
        dishService.bindImage(dishId, id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping(value = "/nearbysearch")
    public ResponseEntity<?> getDishListByNearbyEntertainment(@RequestParam(value = "lat") double latitude,
                                                              @RequestParam(value = "lng") double longitude,
                                                              @RequestParam(value = "name", required = false) String name,
                                                              @RequestParam(value = "cousine", required = false) String cuisine,
                                                              @RequestParam(value = "type", required = false) String type,
                                                              @RequestParam(value = "radius") int radius) {
        return ResponseEntity.status(HttpStatus.OK).body(googleMapService
                .searchDishListOfNearbyEntertainment(googleMapService.searchNearbyPlaces(latitude, longitude, radius),
                                                     name,
                                                     cuisine,
                                                     type)
        );
    }

    @PostMapping(value = "/{id}/emoupdate")
    public ResponseEntity<?> updateEmo(@PathVariable long id,
                                       @RequestParam(value = "emo") double emo) {
        return ResponseEntity.status(HttpStatus.OK).body(dishService.updateEmo(id, emo));
    }

}
