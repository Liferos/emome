package com.ss.emome.controller;

import com.ss.emome.entity.Coordinates;
import com.ss.emome.service.CoordinatesService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Myroslav on 02.01.2018.
 */
@FieldDefaults(level = AccessLevel.PRIVATE)
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "coordinates")
public class CoordinatesController {

    final CoordinatesService coordinatesService;

    @PostMapping
    public ResponseEntity<?> createCoordinates(@RequestBody Coordinates coordinates) {
        return ResponseEntity.status(HttpStatus.CREATED).body(coordinatesService.save(coordinates));
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> deleteCoordinates(@PathVariable long id) {
        coordinatesService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getCoordinates(@PathVariable long id) {
        return ResponseEntity.status(HttpStatus.OK).body(coordinatesService.findOne(id));
    }

    @GetMapping
    public ResponseEntity<?> getCoordinatesList() {
        return ResponseEntity.status(HttpStatus.OK).body(coordinatesService.findAll());
    }

    @GetMapping(value = "/byentertainment")
    public ResponseEntity<?> getCoordinatesByEntertainment(@RequestParam long id) {
        return ResponseEntity.status(HttpStatus.OK).body(coordinatesService.findCoordinatesByEntertainment(id));
    }

    @PutMapping
    public ResponseEntity<?> updateCoordinates(@RequestBody Coordinates alteredCoordinates) {
        coordinatesService.update(alteredCoordinates);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

}
