package com.ss.emome.controller;

import com.ss.emome.entity.Order;
import com.ss.emome.entity.OrderComponent;
import com.ss.emome.service.OrderComponentService;
import com.ss.emome.service.OrderService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Myroslav on 09.01.2018.
 */
@FieldDefaults(level = AccessLevel.PRIVATE)
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "order")
public class OrderController {

    final OrderService orderService;
    final OrderComponentService orderComponentService;

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getOrder(@PathVariable long id) {
        return ResponseEntity.status(HttpStatus.OK).body(orderService.findOne(id));
    }

    @GetMapping
    public ResponseEntity<?> getOrderList() {
        return ResponseEntity.status(HttpStatus.OK).body(orderService.findAll());
    }

    @PostMapping
    public ResponseEntity<?> createOrder(@RequestParam(value = "entertainmentid") long entertainmentId,
                                         @RequestBody String dishListJson) {
        Order tmp = orderService
                .createOrder(new Timestamp(Calendar.getInstance().getTimeInMillis()), entertainmentId);
        List<OrderComponent> orderComponentList = orderComponentService.createOrderComponentList(dishListJson,
                                                                                                 tmp.getId());
        tmp.setTotal(orderComponentList.stream().mapToDouble(t -> t.getAmount() * t.getDish().getPrice()).sum());
        orderService.updateOrder(tmp);
        tmp.setOrderComponents(orderComponentList);
        orderComponentList.forEach(t -> t.getDish().setEntertainment(null));
        return ResponseEntity.status(HttpStatus.CREATED).body(tmp);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> deleteOrder(@PathVariable long id) {
        orderService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping(value = "/byentertainment")
    public ResponseEntity<?> getOrderByEntertainment(@RequestParam long id) {
        return ResponseEntity.status(HttpStatus.OK).body(orderService.findOrderByEntertainment(id));
    }

}