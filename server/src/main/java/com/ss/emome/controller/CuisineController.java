package com.ss.emome.controller;

import com.ss.emome.entity.Cuisine;
import com.ss.emome.service.CuisineService;
import com.ss.emome.service.EntertainmentService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Myroslav on 02.01.2018.
 */
@FieldDefaults(level = AccessLevel.PRIVATE)
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "cuisine")
public class CuisineController {

    final CuisineService cuisineService;

    @PostMapping
    public ResponseEntity<?> createCuisine(@RequestBody Cuisine cuisine) {
        return ResponseEntity.status(HttpStatus.CREATED).body(cuisineService.save(cuisine));
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> deleteCuisine(@PathVariable long id) {
        cuisineService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getCuisine(@PathVariable long id) {
        return ResponseEntity.status(HttpStatus.OK).body(cuisineService.findOne(id));
    }

    @GetMapping
    public ResponseEntity<?> getCuisineList() {
        return ResponseEntity.status(HttpStatus.OK).body(cuisineService.findAll());
    }

    @GetMapping(value = "/byentertainment")
    public ResponseEntity<?> getCuisineByEntertainment(@RequestParam long id) {
        return ResponseEntity.status(HttpStatus.OK).body(cuisineService.findCuisineByEntertainment(id));
    }

    @PutMapping
    public ResponseEntity<?> updateCuisine(@RequestBody Cuisine alteredCuisine) {
        cuisineService.update(alteredCuisine);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

}
