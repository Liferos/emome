package com.ss.emome.controller;

import com.ss.emome.entity.DishType;
import com.ss.emome.service.DishTypeService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Myroslav on 02.01.2018.
 */
@FieldDefaults(level = AccessLevel.PRIVATE)
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "dishtype")
public class DishTypeController {

    final DishTypeService dishTypeService;

    @PostMapping
    public ResponseEntity<?> createDishType(@RequestBody DishType dishType) {
        return ResponseEntity.status(HttpStatus.CREATED).body(dishTypeService.save(dishType));
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> deleteDishType(@PathVariable long id) {
        dishTypeService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getDishType(@PathVariable long id) {
        return ResponseEntity.status(HttpStatus.OK).body(dishTypeService.findOne(id));

    }

    @GetMapping
    public ResponseEntity<?> getDishTypeList() {
        return ResponseEntity.status(HttpStatus.OK).body(dishTypeService.findAll());
    }

    @GetMapping(value = "/bydish")
    public ResponseEntity<?> getDishTypeByDish(@RequestParam long id) {
        return ResponseEntity.status(HttpStatus.OK).body(dishTypeService.findDishTypeByDish(id));
    }

    @PutMapping
    public ResponseEntity<?> updateDishType(@RequestBody DishType alteredDishType) {
        dishTypeService.update(alteredDishType);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

}