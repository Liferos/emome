package com.ss.emome.controller;

import com.ss.emome.entity.Entertainment;
import com.ss.emome.service.EntertainmentService;
import com.ss.emome.service.GoogleMapService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Myroslav on 02.01.2018.
 */
@FieldDefaults(level = AccessLevel.PRIVATE)
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "entertainment")
public class EntertainmentController {

    final EntertainmentService entertainmentService;
    final GoogleMapService googleMapService;

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getEntertainment(@PathVariable long id) {
        return ResponseEntity.status(HttpStatus.OK).body(entertainmentService.findOne(id));
    }

    @GetMapping
    public ResponseEntity<?> getEntertainmentList() {
        return ResponseEntity.status(HttpStatus.OK).body(entertainmentService.findAll());
    }

    @PostMapping
    public ResponseEntity<?> createEntertainment(@RequestBody Entertainment entertainment) {
        return ResponseEntity.status(HttpStatus.CREATED).body(entertainmentService.save(entertainment));
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> deleteEntertainment(@PathVariable long id) {
        entertainmentService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PutMapping
    public ResponseEntity<?> updateEntertainment(@RequestBody Entertainment alteredEntertainment) {
        entertainmentService.update(alteredEntertainment);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping(value = "/bytype")
    public ResponseEntity<?> getEntertainmentByEntertainmentType(@RequestParam long id) {
        return ResponseEntity.status(HttpStatus.OK).body(entertainmentService.findEntertainmentByEntertainmentType(id));
    }

    @GetMapping(value = "/bycuisine")
    public ResponseEntity<?> getEntertainmentByCuisine(@RequestParam long id) {
        return ResponseEntity.status(HttpStatus.OK).body(entertainmentService.findEntertainmentByCuisine(id));
    }

    @GetMapping(value = "/bycoordinates")
    public ResponseEntity<?> getEntertainmentByCoordinates(@RequestParam long id) {
        return ResponseEntity.status(HttpStatus.OK).body(entertainmentService.findEntertainmentByCoordinates(id));
    }

    @PostMapping(value = "/{entertainmentid}/bindcuisine")
    public ResponseEntity<?> bindEntertainmentToCuisine(@PathVariable(value = "entertainmentid") long entertainmentId,
                                                        @RequestParam long id) {
        entertainmentService.bindEntertainmentToCuisine(entertainmentId, id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PostMapping(value = "/{entertainmentid}/bindtype")
    public ResponseEntity<?> bindEtertainmentToEntertainmentType(@PathVariable(value = "entertainmentid") long entertainmentId,
                                                             @RequestParam long id) {
        entertainmentService.bindEntertainmentToEntertainmentType(entertainmentId, id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PostMapping(value = "/{entertainmentid}/bindcoordinates")
    public ResponseEntity<?> bindEntertainmentToCoordinates(@PathVariable(value = "entertainmentid") long entertainmentId,
                                                      @RequestParam long id) {
        entertainmentService.bindEntertainmentToCoordinates(entertainmentId, id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PostMapping(value = "/{entertainmentid}/bindimage")
    public ResponseEntity<?> bindEntertainmentToImage(@PathVariable(value = "entertainmentid") long entertainmentId,
                                                      @RequestParam long id) {
        entertainmentService.bindImage(entertainmentId, id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

}