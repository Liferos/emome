package com.ss.emome.controller;

import com.ss.emome.entity.Comment;
import com.ss.emome.service.CommentService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Myroslav on 02.01.2018.
 */
@FieldDefaults(level = AccessLevel.PRIVATE)
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "comment")
public class CommentController {

    final CommentService commentService;

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getComment(@PathVariable long id) {
        return ResponseEntity.status(HttpStatus.OK).body(commentService.findOne(id));
    }

    @GetMapping
    public ResponseEntity<?> getCommentList() {
        return ResponseEntity.status(HttpStatus.OK).body(commentService.findAll());
    }

    @PostMapping
    public ResponseEntity<?> createComment(@RequestParam(value = "dishid") long id, @RequestBody Comment comment) {
        return ResponseEntity.status(HttpStatus.CREATED).body(commentService.save(id, comment));
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> deleteComment(@PathVariable long id) {
        commentService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping(value = "/bydish")
    public ResponseEntity<?> getCommentsByDish(@RequestParam long id) {
        return ResponseEntity.status(HttpStatus.OK).body(commentService.findCommentsByDish(id));
    }

    @PostMapping(value = "/{commentid}/binddish")
    public ResponseEntity<?> bindCommentToDish(@PathVariable(value = "commentid") long commentId,
                                               @RequestParam long id) {
        commentService.bindCommentToDish(commentId, id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PutMapping
    public ResponseEntity<?> updateComment(@RequestBody Comment alteredComment) {
        commentService.update(alteredComment);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

}
