package com.ss.emome.controller;

import com.ss.emome.entity.Image;
import com.ss.emome.service.ImageService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Myroslav on 18.01.2018.
 */
@FieldDefaults(level = AccessLevel.PRIVATE)
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "image")
public class ImageController {

    final ImageService imageService;

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getImage(@PathVariable long id) {
        return ResponseEntity.status(HttpStatus.OK).body(imageService.findOne(id));
    }

    @GetMapping
    public ResponseEntity<?> getImageList() {
        return ResponseEntity.status(HttpStatus.OK).body(imageService.findAll());
    }

    @GetMapping(value = "/byentertainment")
    public ResponseEntity<?> getImageByEntertainment(@RequestParam(value = "id") long entertainmentId) {
        return ResponseEntity.status(HttpStatus.OK).body(imageService.findByEntertainment(entertainmentId));
    }

    @GetMapping(value = "/bydish")
    public ResponseEntity<?> getImageByDish(@RequestParam(value = "id") long dishId) {
        return ResponseEntity.status(HttpStatus.OK).body(imageService.findByDish(dishId));
    }

    @PostMapping
    public ResponseEntity<?> createImage(@RequestBody Image image) {
        return ResponseEntity.status(HttpStatus.OK).body(imageService.createImage(image));
    }

}
