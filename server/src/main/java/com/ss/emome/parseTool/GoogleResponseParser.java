package com.ss.emome.parseTool;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.ss.emome.entity.POJO.Entertainment;
import com.ss.emome.entityConverter.EntertainmentPojoConverter;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Myroslav on 23.01.2018.
 */
@FieldDefaults(level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
@Component
public class GoogleResponseParser {

    final EntertainmentParser entertainmentParser;
    final CoordinatesParser coordinatesParser;
    final Gson gson;

    private List<Entertainment> parseEntertainmentList(String json) {
        List<Entertainment> result = new ArrayList<>();
        gson.fromJson(json, JsonObject.class).getAsJsonArray("results").forEach(t -> {
            Entertainment tmpEntertainment = entertainmentParser.parseEntertainment(t.getAsJsonObject());
            tmpEntertainment.setLatitude(
                    String.valueOf(coordinatesParser.parseCoordinates(t.getAsJsonObject()).getLatitude()));
            tmpEntertainment.setLongitude(
                    String.valueOf(coordinatesParser.parseCoordinates(t.getAsJsonObject()).getLongitude()));
            result.add(tmpEntertainment);
        });
        return result;
    }

    public List<com.ss.emome.entity.Entertainment> parseResponse(String response) {
        return EntertainmentPojoConverter.fromEntertainmentPOJOList(parseEntertainmentList(response));
    }

}
