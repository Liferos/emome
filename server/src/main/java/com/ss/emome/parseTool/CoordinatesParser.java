package com.ss.emome.parseTool;

import com.google.gson.JsonObject;
import com.ss.emome.entity.POJO.Coordinates;
import org.springframework.stereotype.Component;

/**
 * Created by Myroslav on 23.01.2018.
 */
@Component
public class CoordinatesParser {

    public Coordinates parseCoordinates(JsonObject object) {
        JsonObject jsonCoordinates = object.getAsJsonObject("geometry").getAsJsonObject("location");
        return new Coordinates(jsonCoordinates.get("lat").getAsDouble(), jsonCoordinates.get("lng").getAsDouble());
    }

}
