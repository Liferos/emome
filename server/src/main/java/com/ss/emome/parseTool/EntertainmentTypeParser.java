package com.ss.emome.parseTool;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.ss.emome.entity.POJO.EntertainmentType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Myroslav on 23.01.2018.
 */
@Component
@RequiredArgsConstructor
public class EntertainmentTypeParser {

    final Gson gson;

    public List<EntertainmentType> parseEntertainmentTypes(JsonObject object) {
        List<EntertainmentType> types = new ArrayList<>();
        object.getAsJsonObject().getAsJsonArray("types")
                .iterator().forEachRemaining(t -> types.add(new EntertainmentType(t.toString())));
        return types;
    }

}
