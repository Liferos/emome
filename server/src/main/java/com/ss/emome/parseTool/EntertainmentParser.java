package com.ss.emome.parseTool;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ss.emome.entity.POJO.Entertainment;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Component;

/**
 * Created by Myroslav on 23.01.2018.
 */
@FieldDefaults(level = AccessLevel.PRIVATE)
@Component
@RequiredArgsConstructor
public class EntertainmentParser {

    final Gson gson;

    public Entertainment parseEntertainment(JsonObject object) {
        return gson.fromJson(object, Entertainment.class);
    }

}
