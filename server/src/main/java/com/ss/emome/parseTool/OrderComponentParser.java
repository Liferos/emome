package com.ss.emome.parseTool;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.ss.emome.entity.POJO.OrderComponent;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Myroslav on 25.01.2018.
 */
@FieldDefaults(level = AccessLevel.PRIVATE)
@Component
@RequiredArgsConstructor
public class OrderComponentParser {

    final Gson gson;

    public List<OrderComponent> parseOrderComponent(String orderComponentListJson) {
        List<OrderComponent> result = new ArrayList<>();
        gson.fromJson(orderComponentListJson, JsonObject.class).getAsJsonArray("orderComponents").forEach(
                t -> {
                    result.add(new OrderComponent(t.getAsJsonObject().get("id").getAsLong(),
                                                  t.getAsJsonObject().get("amount").getAsLong()));
                }
        );
        return result;
    }

}
