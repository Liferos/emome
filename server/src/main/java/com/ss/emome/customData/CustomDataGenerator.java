package com.ss.emome.customData;

import com.ss.emome.entity.*;
import com.ss.emome.repository.*;
import com.ss.emome.service.CommentService;
import com.ss.emome.util.RandomUtil;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Created by Myroslav on 01.02.2018.
 */
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Component
public class CustomDataGenerator {

    final EntertainmentRepository entertainmentRepository;
    final DishRepository dishRepository;
    final DishTypeRepository dishTypeRepository;
    final EntertainmentTypeRepository entertainmentTypeRepository;
    final RandomUtil randomUtil;
    final CuisineRepository cuisineRepository;
    final CommentService commentService;

    public List<Dish> getCustomDishListForEntertainment(Entertainment entertainment) {
        Random r = new Random();
        List<Dish> dishList = new ArrayList<>();
        dishList.add(new Dish("Juice \"Mango\"", 1.22, r.nextInt(101), 1, entertainment,
                "https://mafia.ua/storage/editor/fotos/480x0/mangovyj-nektar.png"));
        dishList.add(new Dish("Juice \"Multivitamin\"", 1.22, r.nextInt(101), 1, entertainment,
                "https://mafia.ua/storage/editor/fotos/480x0/multivitamin.png"));
        dishList.add(new Dish("Juice \"Peach\"", 1.22, r.nextInt(101), 1, entertainment,
                "https://mafia.ua/storage/editor/fotos/480x0/persik_1494950847107.png"));
        dishList.add(new Dish("Juice \"Tomato\"", 1.22, r.nextInt(101), 1, entertainment,
                "https://mafia.ua/storage/editor/fotos/480x0/tomat.png"));
        dishList.add(new Dish("Juice \"Apple\"", 1.22, r.nextInt(101), 1, entertainment,
                "https://mafia.ua/storage/editor/fotos/480x0/yabloko.png"));
        dishList.add(new Dish("Juice \"Cherry\"", 1.22, r.nextInt(101), 1, entertainment,
                "https://mafia.ua/storage/editor/fotos/480x0/vishnya_1494953067400.png"));
        dishList.add(new Dish("Juice \"Grapefruit\"", 1.22, r.nextInt(101), 1, entertainment,
                "https://mafia.ua/storage/editor/fotos/480x0/grejpfrut.png"));
        dishList.add(new Dish("Juice \"Red grapes\"", 1.22, r.nextInt(101), 1, entertainment,
                "https://mafia.ua/storage/editor/fotos/480x0/krasnyj-vinorgad_1494951248671.png"));
        dishList.add(new Dish("Juice \"White grapes\"", 1.22, r.nextInt(101), 1, entertainment,
                "https://mafia.ua/storage/editor/fotos/480x0/belyj-vinograd.png"));
        dishList.add(new Dish("Juice \"Orange\"", 1.22, r.nextInt(101), 1, entertainment,
                "https://mafia.ua/storage/editor/fotos/480x0/apelsin_1494953286852.png"));
        dishList.add(new Dish("Mirinda", 0.67, r.nextInt(101), 1, entertainment,
                "https://mafia.ua/storage/editor/fotos/480x0/mirinda-05.png"));
        dishList.add(new Dish("Morshinska carbonated", 1.39, r.nextInt(101), 1, entertainment,
                "https://mafia.ua/storage/editor/fotos/480x0/morshinskaya-033-gaz.png"));
        dishList.add(new Dish("Morshinska", 1.39, r.nextInt(101), 1, entertainment,
                "https://mafia.ua/storage/editor/fotos/480x0/morshinskaya-033-ne-gaz_1494846634640.png"));

        dishList.add(new Dish("Beer \"Beck`s\"", 1.03, r.nextInt(101), 1, entertainment,
                "https://mafia.ua/storage/editor/fotos/480x0/pivo-beks-05-zhb_149510048528.png"));
        dishList.add(new Dish("Beer \"Staropramen\"", 0.85, r.nextInt(101), 1, entertainment,
                "https://mafia.ua/storage/editor/fotos/480x0/staropramen_1495118979871.png"));
        dishList.add(new Dish("Beer \"Stella Artois\"", 1.03, r.nextInt(101), 1, entertainment,
                "https://mafia.ua/storage/editor/fotos/480x0/pivo-stella-artua-05-zhb_1495100670683.png"));

        dishList.add(new Dish("Salad \"Mediterranean\"", 5.31, r.nextInt(101), 1, entertainment,
                "https://mafia.ua/storage/editor/fotos/480x0/sredizemnomorskij-salat_1498043310221.jpeg"));
        dishList.add(new Dish("Salad \"Greek\"", 3.17, r.nextInt(101), 1, entertainment,
                "https://mafia.ua/storage/editor/fotos/480x0/salat-grecheskij_1494837140884.jpeg"));
        dishList.add(new Dish("Salad \"Kaico\"", 2.46, r.nextInt(101), 1, entertainment,
                "https://mafia.ua/storage/editor/fotos/480x0/kajso_1494837760959.jpeg"));
        dishList.add(new Dish("Salad \"Caesar\"", 3.52, r.nextInt(101), 1, entertainment,
                "https://mafia.ua/storage/editor/fotos/480x0/cezar_1494837014239.jpeg"));
        dishList.add(new Dish("Salad with salmon", 4.59, r.nextInt(101), 1, entertainment,
                "https://mafia.ua/storage/editor/fotos/480x0/salat-s-lososem_1496927933407.jpeg"));
        dishList.add(new Dish("Salad with caramelized liver", 3.17, r.nextInt(101), 1, entertainment,
                "https://mafia.ua/storage/editor/fotos/480x0/salat-s-karamelizirovannoj-pechenyu_1506412384191.jpeg"));

        dishList.add(new Dish("Salmon carpaccio", 10.60, r.nextInt(101), 1, entertainment,
                "http://www.ilcanto.ru/images/all/14/3/big/img_8.jpg"));
        dishList.add(new Dish("Beef carpaccio", 9.75, r.nextInt(101), 1, entertainment,
                "http://www.ilcanto.ru/images/all/14/10/big/img_8.jpg"));
        dishList.add(new Dish("Caprese", 8.21, r.nextInt(101), 1, entertainment,
                "http://www.ilcanto.ru/images/all/14/15/big/img_8.jpg"));
        dishList.add(new Dish("Cheese \"Mozzarella\"", 4.62, r.nextInt(101), 1, entertainment,
                "http://www.ilcanto.ru/images/all/14/21/big/img_8.jpg"));
        dishList.add(new Dish("Cheese \"Parmesan\"", 4.44, r.nextInt(101), 1, entertainment,
                "http://www.ilcanto.ru/images/all/14/20/big/img_8.jpg"));
        dishList.add(new Dish("Cheese \"Cheddar\"", 4.27, r.nextInt(101), 1, entertainment,
                "http://www.ilcanto.ru/images/all/14/19/big/img_8.jpg"));
        dishList.add(new Dish("Salami", 4.44, r.nextInt(101), 1, entertainment,
                "http://www.ilcanto.ru/images/all/14/13/big/img_8.jpg"));
        dishList.add(new Dish("Beef", 3.93, r.nextInt(101), 1, entertainment,
                "http://www.ilcanto.ru/images/all/14/146/big/img_8.jpg"));
        dishList.add(new Dish("Olives", 4.79, r.nextInt(101), 1, entertainment,
                "http://www.ilcanto.ru/images/all/14/342/big/img_8.jpg"));
        dishList.add(new Dish("Pepperoni", 3.76, r.nextInt(101), 1, entertainment,
                "http://www.ilcanto.ru/images/all/14/483/big/img_8.jpg"));

        dishList.add(new Dish("Hamburger", 0.78, r.nextInt(101), 1, entertainment,
                "https://www.mcdonalds.ua/content/dam/Ukraine/Item_Images/hero.Sdwch_Hamburger.png"));
        dishList.add(new Dish("Cheeseburger", 0.89, r.nextInt(101), 1, entertainment,
                "https://mcdonalds.ru/system/product_nutritional_values/" +
                        "images/000/000/012/big/cheeseburger_big-800.jpg?1495107960"));
        dishList.add(new Dish("Double cheeseburger", 1.67, r.nextInt(101), 1, entertainment,
                "https://www.mcdonalds.ua/content/dam/Ukraine/Item_Images/hero.Sdwch_DoubleCheeseburger.png"));
        dishList.add(new Dish("Big tasty", 3.81, r.nextInt(101), 1, entertainment,
                "https://www.mcdonalds.ua/content/dam/Ukraine/Item_Images/hero.Sdwch_BigTasty.png"));

        dishList.add(new Dish("Set \"Philadelphia\"", 17.79, r.nextInt(101), 1, entertainment,
                "https://mafia.ua/storage/editor/fotos/480x0/filadelfiya-set_149695750085.jpeg"));
        dishList.add(new Dish("Set \"California\"", 14.22, r.nextInt(101), 1, entertainment,
                "https://mafia.ua/storage/editor/fotos/480x0/kaliforniya-set_1496957673918.jpeg"));
        dishList.add(new Dish("Set \"Premium\"", 20.28, r.nextInt(101), 1, entertainment,
                "https://mafia.ua/storage/editor/fotos/480x0/premium-set_1496957628502.jpeg"));
        dishList.add(new Dish("Set \"Susy\"", 10.66, r.nextInt(101), 1, entertainment,
                "https://mafia.ua/storage/editor/fotos/480x0/susi-set_1496957593910.jpeg"));
        dishList.add(new Dish("Set \"Grand\"", 15.65, r.nextInt(101), 1, entertainment,
                "https://mafia.ua/storage/editor/fotos/480x0/grand-set_1506005826192.jpeg"));

        dishList.add(new Dish("Cake \"Company\"", 3.52, r.nextInt(101), 1, entertainment,
                "https://mafia.ua/storage/editor/fotos/480x0/tort-firmennyj_1505988156811.jpeg"));
        dishList.add(new Dish("Crimson cheesecake", 2.46, r.nextInt(101), 1, entertainment,
                "https://mafia.ua/storage/editor/fotos/480x0/chizkejk-malinovyj_1505987061520.jpeg"));
        dishList.add(new Dish("Cheesecake", 2.1, r.nextInt(101), 1, entertainment,
                "https://mafia.ua/storage/editor/fotos/480x0/chizkejk_1494660616483.jpeg"));
        dishList.add(new Dish("Profiteroles", 3.17, r.nextInt(101), 1, entertainment,
                "https://mafia.ua/storage/editor/fotos/480x0/profitroli_1509357543606.jpeg"));
        dishList.add(new Dish("Alba Rosso", 3.17, r.nextInt(101), 1, entertainment,
                "https://mafia.ua/storage/editor/fotos/480x0/alba-rosso_1502353357175.jpeg"));

        dishList.add(new Dish("Sauce \"Bavarian\"", 0.35, r.nextInt(101), 1, entertainment,
                "http://www.pizza-celentano.kiev.ua/assets/cache/images/soysa/285x285-2-1.8c2.jpg"));
        dishList.add(new Dish("Sauce \"Caesar\"", 0.35, r.nextInt(101), 1, entertainment,
                "http://www.pizza-celentano.kiev.ua/assets/cache/images/soysa/285x285-5_1.8c2.jpg"));
        dishList.add(new Dish("Sauce \"For beer\"", 0.35, r.nextInt(101), 1, entertainment,
                "http://www.pizza-celentano.kiev.ua/assets/cache/images/soysa/285x285-4_1.8c2.jpg"));
        dishList.add(new Dish("Sauce \"Cream\"", 0.35, r.nextInt(101), 1, entertainment,
                "http://www.pizza-celentano.kiev.ua/assets/cache/images/soysa/285x285-3_1.8c2.jpg"));
        dishList.add(new Dish("Sauce \"Pesto\"", 0.35, r.nextInt(101), 1, entertainment,
                "http://www.pizza-celentano.kiev.ua/assets/cache/images/soysa/285x285-sous.617.png"));

        dishList.add(new Dish("William cola", 10.33, r.nextInt(101), 1, entertainment,
                "https://media.maximilians.ru/kazan/cuisine/vilyam-kola/vilyam-kola-min.jpg"));
        dishList.add(new Dish("Bacardi mojito", 12.83, r.nextInt(101), 1, entertainment,
                "https://media.maximilians.ru/kazan/cuisine/mohito-alkogolnyj/bakardi-mohito-min.jpg"));
        dishList.add(new Dish("Bacardi Cuba libre", 12.83, r.nextInt(101), 1, entertainment,
                "https://media.maximilians.ru/kazan/cuisine/bakardi-kuba-libre/bakardi-kuba-libre-min.jpg"));
        dishList.add(new Dish("Watermelon crust", 10.69, r.nextInt(101), 1, entertainment,
                "https://media.maximilians.ru/kazan/cuisine/bakardi-kuba-libre/abuznaya-korka-min.jpg"));
        dishList.add(new Dish("Apple Jameson", 12.83, r.nextInt(101), 1, entertainment,
                "https://media.maximilians.ru/kazan/cuisine/bakardi-kuba-libre/yablochnyj-dzhemeson-min.jpg"));
        dishList.add(new Dish("Oakhart cola", 10.69, r.nextInt(101), 1, entertainment,
                "https://media.maximilians.ru/kazan/cuisine/bakardi-kuba-libre/oakhart-kola-min.jpg"));
        dishList.add(new Dish("Green day", 9.98, r.nextInt(101), 1, entertainment,
                "https://media.maximilians.ru/kazan/cuisine/bakardi-kuba-libre/grin-dej-min.jpg"));
        dishList.add(new Dish("Princess Street", 11.76, r.nextInt(101), 1, entertainment,
                "https://media.maximilians.ru/kazan/cuisine/bakardi-kuba-libre/prinses-strit-min.jpg"));

        dishList.add(new Dish("Broccoli cream soup", 1.60, r.nextInt(101), 1, entertainment,
                "https://eda.ua/images/152753-220-184-brokkoli.jpg"));
        dishList.add(new Dish("Salad \"Green\"", 2.31, r.nextInt(101), 1, entertainment,
                "https://eda.ua/images/152776-220-184-salat_zelenuy.jpg"));
        dishList.add(new Dish("Mushroom pasta", 1.84, r.nextInt(101), 1, entertainment,
                "https://eda.ua/images/156089-220-184-pasta_s_gribami.jpg"));

        dishList.add(new Dish("Vine \"Cynandali\"", 9.26, r.nextInt(101), 1, entertainment,
                "http://www.pizza-celentano.kiev.ua/assets/cache/" +
                        "images/napitki/444x444-file_3977518__47286_orig.93f.png"));
        dishList.add(new Dish("Vine \"Saperavi\"", 9.26, r.nextInt(101), 1, entertainment,
                "http://www.pizza-celentano.kiev.ua/assets/cache/" +
                        "images/napitki/444x444-saperavi_red_0_0__55194_orig.11c.jpg"));
        dishList.add(new Dish("Cognac \"Tavria\"", 10.69, r.nextInt(101), 1, entertainment,
                "http://www.pizza-celentano.kiev.ua/assets/cache/" +
                        "images/napitki/444x444-tavriya.93f.png"));
        dishList.add(new Dish("Vodka \"Nemiroff\" with pepper", 5.7, r.nextInt(101), 1,
                entertainment, "http://www.pizza-celentano.kiev.ua/assets/cache/" +
                "images/napitki/444x444-gorkaya-medovaya.11c.jpg"));

        dishList.add(new Dish("Pizza \"Americano\"", 6.23, r.nextInt(101), 1, entertainment,
                "http://www.pizza-celentano.kiev.ua/assets/cache/images/444x444-amerikano-new1.11c.jpg"));
        dishList.add(new Dish("Pizza \"Bavarian\"", 6.23, r.nextInt(101), 1, entertainment,
                "http://www.pizza-celentano.kiev.ua/assets/cache/images/444x444-bavarskaya-new.11c.jpg"));
        dishList.add(new Dish("Pizza \"4 cheeses\"", 6.23, r.nextInt(101), 1, entertainment,
                "http://www.pizza-celentano.kiev.ua/assets/cache/images/444x444-chotiri-siri1.11c.jpg"));
        dishList.add(new Dish("Pizza \"Hawaiian\"", 4.81, r.nextInt(101), 1, entertainment,
                "http://www.pizza-celentano.kiev.ua/assets/cache/images/444x444-gavayskaya-new.11c.jpg"));
        dishList.add(new Dish("Pizza \"Margaret\"", 3.92, r.nextInt(101), 1, entertainment,
                "http://www.pizza-celentano.kiev.ua/assets/cache/images/444x444-margarina-new.11c.jpg"));
        dishList.add(new Dish("Pizza \"Diabola\"", 5.52, r.nextInt(101), 1, entertainment,
                "http://www.pizza-celentano.kiev.ua/assets/cache/images/444x444-diabola-new.11c.jpg"));

        return dishList;
    }

    private String getRandomPhrase() {
        List<String> phrases = Arrays.asList(
                "Just to say \"thank you\" for our meal last Saturday. We all enjoyed your delicious food and the " +
                        "evening was everything we hoped it would be. Thank you so much.",
                "We went for a meal to celebrate a birthday, and as usual had a fantastic meal. Great service, great " +
                        "food and great value for money. We recommend Longfellows to people.",
                "Excellent environment, friendly service, great menu choices and really decent food and at affordable " +
                        "prices. Will be back.",
                "What a lovely restaurant, good food and service and atmosphere, definitely hope to go back. " +
                        "The roasties were YUMMY!!!",
                "A warm and friendly welcome with fantastic customer service. Always great, tasty food served piping hot " +
                        "just the way we like it. Would definitely recommend. We have been many times and it has consistently exceeded our expectations.",
                "A tremendous evening was enjoyed by our party of six, very good food, polite and friendly staff, what " +
                        "more can one ask for. Recommend anyone to give this superb restaurant a try.",
                "Many thanks for a splendid meal and night out for my colleagues and I, whilst working at the NEC. We " +
                        "ate excellent food and had a very warm and friendly welcome. Sure to visit again on our next works trip!",
                "Superb food. Natural ambience. One of the few independent restaurants worth going to",
                "Our fourth Company Christmas Dinner and once more the quality of food and service continues to be of " +
                        "the most exceptional Standard.",
                "I would just like to say thank you for the excellent service we received in your restaurant last night. " +
                        "Although the place was very busy we still had a great time and really appreciated the live entertainment too!"
        );
        return phrases.get(randomUtil.getRandIntBySpecifiedRange(0, phrases.size()));
    }

    private String getRandomAuthor() {
        List<String> authors = Arrays.asList(
                "Bailey", "Bella", "Max", "Lucy", "Charlie", "Molly", "Buddy", "Daisy", "Rocky", "Maggie",
                "Jake", "Sophie", "Jack", "Sadie", "Toby", "Chloe", "Cody", "Bailey", "Buster", "Lola", "Duke",
                "Zoe", "Cooper", "Abby", "Riley", "Ginger", "Harley", "Roxy", "Bear", "Gracie", "Tucker", "Coco",
                "Murphy", "Sasha", "Lucky", "Lily", "Oliver", "Angel", "Sam", "Princess", "Oscar",
                "Emma", "Teddy", "Annie", "Winston", "Rosie", "Sammy", "Ruby"
        );
        return authors.get(randomUtil.getRandIntBySpecifiedRange(0, authors.size()));
    }

    private List<Comment> getRandomCommentListWithNullableDish() {
        int size = randomUtil.getRandIntBySpecifiedRange(0, 50);
        List<Comment> comments = new ArrayList<>();
        while(comments.size() <= size) {
            comments.add(new Comment(getRandomPhrase(), getRandomAuthor(), null));
        }
        return comments;
    }

    public void bindCustomCommentList(List<Dish> dishList) {
        dishList
                .forEach(
                        dish -> {
                            getRandomCommentListWithNullableDish()
                                    .forEach(comment -> {
                                        comment.setDish(dish);
                                        commentService.save(dish.getId(), comment);
                                    });

                        }
                );
    }

    public void bindCustomDishList(List<Dish> dishList) {
        for(int i=0;i<dishList.size();i++) {
            if(i <= 12) {
                dishRepository.bindDishType(dishRepository.save(dishList.get(i)).getId(),
                        dishTypeRepository.findDishTypeByTitle("Drinks").getId());
            } else if(i <= 15) {
                dishRepository.bindDishType(dishRepository.save(dishList.get(i)).getId(),
                        dishTypeRepository.findDishTypeByTitle("Beer").getId());
            } else if(i <= 21) {
                dishRepository.bindDishType(dishRepository.save(dishList.get(i)).getId(),
                        dishTypeRepository.findDishTypeByTitle("Salads").getId());
            } else if(i <= 31) {
                dishRepository.bindDishType(dishRepository.save(dishList.get(i)).getId(),
                        dishTypeRepository.findDishTypeByTitle("Snacks").getId());
            } else if(i <= 35) {
                dishRepository.bindDishType(dishRepository.save(dishList.get(i)).getId(),
                        dishTypeRepository.findDishTypeByTitle("Burgers").getId());
            } else if(i <= 40) {
                dishRepository.bindDishType(dishRepository.save(dishList.get(i)).getId(),
                        dishTypeRepository.findDishTypeByTitle("Sushi").getId());
            } else if(i <= 45) {
                dishRepository.bindDishType(dishRepository.save(dishList.get(i)).getId(),
                        dishTypeRepository.findDishTypeByTitle("Desserts").getId());
            } else if(i <= 50) {
                dishRepository.bindDishType(dishRepository.save(dishList.get(i)).getId(),
                        dishTypeRepository.findDishTypeByTitle("Sauces").getId());
            } else if(i <= 58) {
                dishRepository.bindDishType(dishRepository.save(dishList.get(i)).getId(),
                        dishTypeRepository.findDishTypeByTitle("Cocktails").getId());
            } else if(i <= 61) {
                dishRepository.bindDishType(dishRepository.save(dishList.get(i)).getId(),
                        dishTypeRepository.findDishTypeByTitle("Vegan").getId());
            } else if(i <= 65) {
                dishRepository.bindDishType(dishRepository.save(dishList.get(i)).getId(),
                        dishTypeRepository.findDishTypeByTitle("Alcoholic drinks").getId());
            } else if(i <= 71) {
                dishRepository.bindDishType(dishRepository.save(dishList.get(i)).getId(),
                        dishTypeRepository.findDishTypeByTitle("Pizza").getId());
            }
        }
    }

    public void bindRandomEntertainmentType(Entertainment entertainment) {
        List<EntertainmentType> entertainmentTypeList = entertainmentTypeRepository.findAll();
        int typeId;
        for(int i = 0; i < 2; i++) {
            typeId = randomUtil.getRandIntBySpecifiedRange(0, entertainmentTypeList.size());
            if(!entertainmentRepository.entertainmentTypeRelationshipExists(entertainment.getId(),
                                                                            entertainmentTypeList.get(typeId).getId()))
                entertainmentRepository.bindType(entertainment.getId(), entertainmentTypeList.get(typeId).getId());

        }
    }

    public void bindRandomCuisine(Entertainment entertainment) {
        List<Cuisine> cuisineList = cuisineRepository.findAll();
        int cuisineId;
        for(int i=0; i < 2; i++) {
            cuisineId = randomUtil.getRandIntBySpecifiedRange(0, cuisineList.size());
            if(!entertainmentRepository.entertainmentCuisineRelationshipExists(entertainment.getId(),
                                                                               cuisineList.get(cuisineId).getId())) {
                entertainmentRepository.bindCuisine(entertainment.getId(), cuisineList.get(cuisineId).getId());
            }
        }
    }

}